from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
# Create your models here.
class UserProfile(models.Model):

    PRIORITY = (
         ('Gold', 'Gold'),
         ('Silver', 'Silver'),
         ('Bronze', 'Bronze'),
    )
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    priority = models.CharField(max_length=10, choices=PRIORITY, default='Bronze')

    def __str__(self):
        return self.user.username

# @receiver(post_save, sender=User)
# def create_user_profile(sender, instance, created, **kwargs):
#     if created:
#         UserProfile.objects.create(user=instance)
#
# @receiver(post_save, sender=User)
# def save_user_profile(sender, instance, **kwargs):
#     instance.profile.save()
