from django.shortcuts import render, HttpResponse, redirect
from django.template import loader
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from django.contrib import auth
from django.contrib.auth.decorators import login_required

from pymongo import MongoClient
import gridfs
from bson.objectid import ObjectId
connection_string = "mongodb://localhost"

from .models import UserProfile
# Create your views here.
@login_required(login_url='/accounts/login/')
def profile(request):
    if request.user.is_authenticated():
        user = request.user
    userProfile = UserProfile.objects.get(user=user)
    template = loader.get_template('userAccount/profile.html')

    conn = MongoClient(connection_string)
    db = conn.arcadiaGateway
    files = db.fs.files.find({"username" : "arcadia", "transcodedFile" : False})
    transcodedFiles = db.fs.files.find({"username" : "arcadia", "transcodedFile" : True})
    conn.close()
    files = [file for file in files]
    transcodedFiles = [file for file in transcodedFiles]
    total = len(files)
    pending = total - len(transcodedFiles)
    transcoded = total - pending

    transcodedObjects = []
    for f in files:
        file = {}
        file = f
        file['id'] = file['_id']
        del file['_id']
        for i in range(0,len(transcodedFiles)):
            if file['transcodeID'] == transcodedFiles[i]['transcodeID']:
                file['transcodedFile'] = transcodedFiles.pop(i)
                #transcodedObjects.append(file)
                break
        if file['transcodedFile'] == False or file['transcodedFile'] == True:
            file['transcodedFile'] = None
        transcodedObjects.append(file)

    print(transcodedObjects)

    context = {
        "active" : "profile",
        "userProfile" : userProfile,
        "user" : user,
        "total" : total,
        "pending" : pending,
        "transcoded" : transcoded,
        "transcodedObjects" : transcodedObjects
    }
    return HttpResponse(template.render(context, request))

def login(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        print(user)
        if user is not None:
            if not user.is_superuser:
                auth.login(request, user)
                return redirect("/")
        template = loader.get_template('userAccount/login.html')
        context = {'message' : "Username or Password is wrong"}
        return HttpResponse(template.render(context, request))
    template = loader.get_template('userAccount/login.html')
    context = {'message' : ""}
    return HttpResponse(template.render(context, request))

def logout(request):
    auth.logout(request)
    template = loader.get_template('userAccount/login.html')
    context = {'message' : ""}
    return HttpResponse(template.render(context, request))
