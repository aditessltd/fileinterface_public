from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^object/(?P<id>[0-9a-z-]+)?$', views.transcodeObject, name='transcodeObject'),
    url(r'^object/(?P<id>[0-9a-z-]+)?/$', views.transcodeObject, name='transcodeObject'),

    url(r'^object/download/(?P<id>[0-9a-z-]+)?$', views.downloadVideo, name='downloadVideo'),
    url(r'^object/download/(?P<id>[0-9a-z-]+)?/$', views.downloadVideo, name='downloadVideo'),

    url(r'^objects$', views.objects, name='objects'),
    url(r'^objects/$', views.objects, name='objects'),

    url(r'^upload/$', views.upload, name='index'),
]
