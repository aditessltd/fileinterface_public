from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required

from django.template import loader
import uuid
from pymongo import MongoClient
import gridfs
from bson.objectid import ObjectId
from userAccount.models import UserProfile

from FileInterface.settings import CONN_STRING

# Create your views here.
@login_required(login_url='/accounts/login/')
def index(request):
    if request.user.is_authenticated():
        user = request.user
    userProfile = UserProfile.objects.get(user=user)
    template = loader.get_template('dashboard/index.html')

    conn = MongoClient(CONN_STRING)
    db = conn.arcadiaGateway
    files = db.fs.files.find({"username" : userProfile.user.username, "transcodedFile" : False}).sort([("_id" , -1)]).limit(9).skip(0)
    transcodedFiles = db.fs.files.find({"username" : userProfile.user.username, "transcodedFile" : True}).limit(9).skip(0)
    conn.close()
    files = [file for file in files]
    transcodedFiles = [file for file in transcodedFiles]
    #for file in files:
    #print("files: ",files)
    #print("tfiles: ",transcodedFiles)
    #total = db.fs.files.count({"username" : "arcadia", "transcodedFile" : False})
    #pending = total - db.fs.files.count({"username" : "arcadia", "transcodedFile" : True})
    #transcoded = total - pending

    total = 0
    pending = 0
    transcoded = 0

    transcodedObjects = []
    for f in files:
        total = total + 1
        file = {}
        file = f
        file['id'] = file['_id']
        del file['_id']
        for i in range(0,len(transcodedFiles)):
            if file['transcodeID'] == transcodedFiles[i]['transcodeID']:
                transcoded = transcoded + 1
                file['transcodedFile'] = transcodedFiles.pop(i)
                #transcodedObjects.append(file)
                break
        if file['transcodedFile'] == False or file['transcodedFile'] == True:
            #continue
            file['transcodedFile'] = None
        transcodedObjects.append(file)

    pending = total - transcoded
    context = {
        "active" : "dashboard",
        "userProfile" : userProfile,
        "user" : user,
        "total" : total,
        "pending" : pending,
        "transcoded" : transcoded,
        "transcodedObjects" : transcodedObjects
    }
    return HttpResponse(template.render(context, request))

    if request.user.is_authenticated():
        user = request.user
    conn = MongoClient(CONN_STRING)
    db = conn.arcadiaGateway
    files = db.fs.files.find({"username" : user.username})
    files = [{"name" : file['filename'], "id" : file['_id']} for file in files]
    print(files)
    conn.close()
    template = loader.get_template('dashboard/index.html')
    context = {
        "files" : files,
        "user" : user,
        "active" : "dashboard"
    }
    return HttpResponse(template.render(context, request))

@login_required(login_url='/accounts/login/')
def objects(request):
    if request.user.is_authenticated():
        user = request.user
    userProfile = UserProfile.objects.get(user=user)
    template = loader.get_template('dashboard/transcodeObjects.html')

    conn = MongoClient(CONN_STRING)
    db = conn.arcadiaGateway
    files = db.fs.files.find({"username" : userProfile.user.username, "transcodedFile" : False})#.sort([("_id" , -1)]).limit(9).skip(0)
    transcodedFiles = db.fs.files.find({"username" : userProfile.user.username, "transcodedFile" : True})#.limit(9).skip(0)
    conn.close()
    files = [file for file in files]
    transcodedFiles = [file for file in transcodedFiles]
    #for file in files:
    #print("files: ",files)
    #print("tfiles: ",transcodedFiles)
    #total = len(files)
    #pending = total - len(transcodedFiles)
    #transcoded = total - pending

    total = 0
    pending = 0
    transcoded = 0

    transcodedObjects = []
    for f in files:
        total = total + 1
        file = {}
        file = f
        file['id'] = file['_id']
        del file['_id']
        for i in range(0,len(transcodedFiles)):
            if file['transcodeID'] == transcodedFiles[i]['transcodeID']:
                transcoded = transcoded + 1
                file['transcodedFile'] = transcodedFiles.pop(i)
                #transcodedObjects.append(file)
                break
        if file['transcodedFile'] == False or file['transcodedFile'] == True:
            #continue
            file['transcodedFile'] = None
        transcodedObjects.append(file)

    #print(transcodedObjects)
    pending = total - transcoded
    context = {
        "active" : "objects",
        "userProfile" : userProfile,
        "user" : user,
        "total" : total,
        "pending" : pending,
        "transcoded" : transcoded,
        "transcodedObjects" : transcodedObjects
    }
    return HttpResponse(template.render(context, request))

    if request.user.is_authenticated():
        user = request.user
    conn = MongoClient(CONN_STRING)
    db = conn.arcadiaGateway
    files = db.fs.files.find({"username" : user.username})
    files = [{"name" : file['filename'], "id" : file['_id']} for file in files]
    print(files)
    conn.close()
    template = loader.get_template('dashboard/index.html')
    context = {
        "files" : files,
        "user" : user,
        "active" : "dashboard"
    }
    return HttpResponse(template.render(context, request))

@login_required(login_url='/accounts/login/')
def downloadVideo(request, id):
    transcoded = request.GET.get('transcoded', False)
    if transcoded == 'true' or transcoded == 'True':
        transcoded = True
    if transcoded == 'false' or transcoded == 'False':
        transcoded = False

    conn = MongoClient(CONN_STRING)
    db = conn.arcadiaGateway
    fs = gridfs.GridFS(db)
    files = fs.find({"transcodeID" : id, "transcodedFile" : transcoded})
    conn.close()
    files = [file for file in files]

    filename = files[0].filename
    response = HttpResponse(content_type="video/" + files[0].transcodeFormat)
    response['Content-Disposition'] = 'attachment; filename=%s' % filename # force browser to download file
    response.write(files[0].read())
    return response

@login_required(login_url='/accounts/login/')
def transcodeObject(request, id):
    # transcoded = request.GET.get('transcoded', False)
    # if transcoded == 'true' or transcoded == 'True':
    #     transcoded = True
    # if transcoded == 'false' or transcoded == 'False':
    #     transcoded = False

    conn = MongoClient(CONN_STRING)
    db = conn.arcadiaGateway
    fs = gridfs.GridFS(db)
    files = fs.find({"transcodeID" : id})
    conn.close()
    #files = [{"filename" : file.filename, "trancodeFormat" : file.trancodeFormat, "priority": file.priority, "uploadDate" : file.uploadDate} for file in files]
    files = [file for file in files]
    file = {"filename" : files[0].filename, "transcodeFormat" : files[0].transcodeFormat, "priority": files[0].priority, "uploadDate" : files[0].uploadDate, "transcodeID" : files[0].transcodeID}
    print(len(files))
    if len(files) == 2:
        file['transcodedFile'] = files[1]
    else:
        file['transcodedFile'] = None
    #file['transcodedFile'] = None
    #files[0]['transcodedFile'] = files[1]
    #filename = files[0].filename
    #response = HttpResponse(content_type="video/" + files[0].transcodeFormat)
    #response['Content-Disposition'] = 'attachment; filename=%s' % filename # force browser to download file
    #response.write(files[0].read())
    #return response

    template = loader.get_template('dashboard/transcodeObject.html')
    context = {
        "file" : file,#files[0],
        "active" : "objects"
    }
    return HttpResponse(template.render(context, request))

@login_required(login_url='/accounts/login/')
def upload(request):
    #print(request.FILES)
    try:
        #request.FILES['file']
        if request.method == 'POST' and request.FILES['image_file']:
            if request.user.is_authenticated():
                user = request.user
            upload = request.FILES['image_file']
            file_data = b''
            for chunk in upload.chunks():
                file_data = file_data + chunk
            #print(file_data)
            conn = MongoClient(CONN_STRING)
            db = conn.arcadiaGateway
            files = gridfs.GridFS(db)
            #fp = open(request.FILES['file'])
            #print(len(request.FILES['file'].read()))
            id = files.put(file_data, filename=request.FILES['image_file'].name)
            userProfile = UserProfile.objects.filter(user=user.id)[0]
            metadata = {
                "username" : user.username,
                "priority" : userProfile.priority,
                "transcodeID": str(uuid.uuid4()),
                "transcodeFormat": "3gp",
                "transcodedFile": False,
            }
            db.fs.files.update({"_id" : ObjectId(str(id))}, { "$set" : metadata })
            #fp.close()
            conn.close()
            #from subprocess import call
            #call(['hadoop', 'jar', '/home/nectarios/Projects/fileinterface/src/transcode.jar', str(id)])
        return HttpResponseRedirect("/dashboard")
        return redirect('/dashboard/')
    except Exception as e:
        print(str(e))
        return HttpResponse(str(e), status=400)
    #template = loader.get_template('dashboard/index.html')
    #context = {}
    #return HttpResponse(template.render(context, request))
